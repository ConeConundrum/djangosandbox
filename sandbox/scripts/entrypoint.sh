#!/bin/bash

sleep 10

cd sandbox || exit
python3 manage.py collectstatic --no-input --clear
python3 manage.py makemigrations
python3 manage.py migrate --no-input
gunicorn --config gunicorn.conf.py --reload sandbox.wsgi