from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status


class AccountsJWTTests(APITestCase):

    def setUp(self) -> None:

        self.urls = {
            'auth': reverse('api-jwt-auth'),
            'refresh': reverse('api-jwt-refresh'),
            'verify': reverse('api-jwt-verify'),
        }
        self.user = User.objects.create_user(
            username='test_user',
            email='test_user@test.com',
            password='password123',
        )

    def test_obtain_jwt(self) -> None:

        url = self.urls['auth']

        # Test wrong user
        data = {
            'username': 'test',
            'email': 'user@foo.com',
            'password': 'pass'
        }
        resp = self.client.post(url, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

        # Test inactive user
        self.user.is_active = False
        self.user.save()

        data = {
            'username': self.user.username,
            'email': self.user.email,
            'password': 'password123',
        }
        resp = self.client.post(url, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

        # Test active user
        self.user.is_active = True
        self.user.save()
        data = {
            'username': self.user.username,
            'email': self.user.email,
            'password': 'password123',
        }
        resp = self.client.post(url, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in resp.data)

    def test_refresh_jwt(self) -> None:

        url_refresh = self.urls['refresh']
        url_auth = self.urls['auth']
        data = {
            'username': self.user.username,
            'email': self.user.email,
            'password': 'password123',
        }

        resp = self.client.post(url_auth, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in resp.data)
        token_orig = resp.data['token']

        data = {
            'token': token_orig
        }

        resp = self.client.post(url_refresh, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in resp.data)

    def test_verify_jwt(self) -> None:

        url_auth = self.urls['auth']
        url_verify = self.urls['verify']

        data = {
            'username': self.user.username,
            'email': self.user.email,
            'password': 'password123',
        }

        resp = self.client.post(url_auth, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in resp.data)
        token_orig = resp.data['token']

        data = {
            'token': token_orig
        }

        resp = self.client.post(url_verify, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in resp.data)
        new_token = resp.data['token']
        self.assertEqual(new_token, token_orig)

        data = {
            'token': token_orig[:-1]
        }
        resp = self.client.post(url_verify, data=data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)



